package br.com.itau;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {

	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
		
		//TODO: continue daqui
//		System.out.println("Hello");

		lancamentos.stream()
				.sorted(Comparator.comparing(Lancamento::getMes))
				.collect(Collectors.toList())
				.forEach(System.out::println);

		System.out.println(lancamentos.stream()
				.collect(Collectors.groupingBy(Lancamento::getCategoria,Collectors.toList()))
				.get(1));

		System.out.println("O total de Janeiro é: " + lancamentos.stream()
				.collect(Collectors.groupingBy(Lancamento::getMes,Collectors.toList()))
				.get(1).stream().mapToDouble(Lancamento::getValor)
				.reduce(0, (total, currentValue) -> total + currentValue));
	}
}
